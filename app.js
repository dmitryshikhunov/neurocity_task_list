var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require("body-parser");

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const mysql = require("mysql");
  
const connection = mysql.createConnection({
  host: "localhost",
  user: "admin",
  database: "neurocity",
  password: "admin"
});

app.get('/', function () {
  res.sendFile(__dirname + '/public/index.html');
})

app.post('/taskcreate', function (req, res) {
  connection.connect(function(err) {
    var insert = "INSERT INTO task_list (`status`, `title`, `description`) VALUES (0, '" + req.body.title + "','" + req.body.description + "');";
    connection.query(insert, function(err, results) {
      if (err) {
        res.end();
        throw err;
      } else {
        res.send({'id' : results.insertId});
        res.end();
      }
    });
  });
});

app.post('/taskupdate', function (req, res) {
  connection.connect(function(err) {
    var update = "UPDATE task_list SET `status` ="+req.body.status+", `title`='" +req.body.title +"',`description`='" + req.body.description +"' WHERE `id` = " + req.body.id + ";";
    console.log(update);
    connection.query(update, function(err, results) {
      if (err) {
        res.end();
        throw err;
      } else {
        res.send({'test' : 'resp'});
        res.end();
      }
    });
  });
});

app.post('/taskdelete', function (req, res) {
  connection.connect(function(err) {
    var deleted = "DELETE FROM task_list WHERE id=" + req.body.id + ";";
    connection.query(deleted, function(err, results) {
      if (err) {
        res.end();
        throw err;
      } else {
        res.send({'test':'resp'});
        res.end();
      }
    });
  });
});

app.get('/tasksget', function (req, res) {
  connection.connect(function(err) {
    var select = "SELECT * FROM task_list;";
    connection.query(select, function(err, results) {
      if (err) {
        res.end();
        throw err;
      } else {
        res.send({'res' : results});
        res.end();
      }
    });
  });
});


app.listen(8080, function () {
  console.log('Тест. Порт: 8080');
});