$(function () {
    var taskList = $('.task-list'),
        lastTask = Object,
        taskTitle = $('.task-title'),
        taskDescription = $('.task-description'),
        taskStatus = $('.task-status'),
        taskManager = {
        'create': function(title, description) {
            var id;
            $.ajax({
                  type: 'POST',
                  url: 'http://localhost:8080/taskcreate/',
                  data: {'title' : title, 'description' : description},
                  success: function(data){
                    id = data.id;
                    if (!id) {
                        return;
                    };
                    var newTask = $('<div class="task">\
                        <div class="t-id">' + id + '</div>\
                        <div class="t-title">' + title + '</div>\
                        <div class="t-status" value="0">Открыта</div>\
                        <div class="t-description">' + description + '</div>\
                    </div>')
                    taskList.prepend(newTask);
                    reloadEvent();
                  }
            })
        },
        'delete': function(e) {
            var id = $(lastTask.element).find('.t-id').text();
            $.ajax({
                type: 'POST',
                url: 'http://localhost:8080/taskdelete/',
                data: {'id' : id},
                success: function(data){
                    lastTask.element.detach();
                }}
            )}
            ,
        'update': function() {
            if (!lastTask.element) {
                return;
            }
            $.ajax({
                type: 'POST',
                url: 'http://localhost:8080/taskupdate/',
                data: {
                    'id'         : lastTask.id.text(),
                    'title'      : taskTitle.val(),
                    'status'     : taskStatus.val(),
                    'description': taskDescription.val()
                },
                success: function(data){
                    if (/\S/.test(taskTitle.val().slice(0, 25))) lastTask.title.text(taskTitle.val().slice(0, 25));
                    if (/\S/.test(taskDescription.val().slice(0, 60))) lastTask.description.text(taskDescription.val().slice(0, 60));
                    if (/\S/.test(taskStatus.val())) lastTask.status.attr('value', taskStatus.val());
                    lastTask.status.text(taskStatus.children().eq(taskStatus.val()).text());
                }
          })
        }
    }

    reloadEvent = function() {
        var tasks = $('.task');
        tasks.unbind('click');
        tasks.click(function(e) {
            if (lastTask.element) {
                lastTask.element.css('background-color', '');
            }
            var task = $(e.currentTarget);
            lastTask.title = task.find('.t-title');
            lastTask.description = task.find('.t-description');
            lastTask.status = task.find('.t-status');
            lastTask.id = task.find('.t-id');
            task.css('background-color', 'lightblue');
            taskTitle.val(lastTask.title.text());
            taskDescription.val(lastTask.description.text());
            taskStatus.val(lastTask.status.attr('value'));
            lastTask.element = task;
        });
    }

    $('#create').click(function(e) {
        taskManager.create('Новая задача', 'Описание новой задачи');
    });
    
    $('#delete').click(function() {
        taskManager.delete();
    });
    $('#save').click(function() {
        taskManager.update();
    });

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/tasksget/',
        success: function(data) {
            $(data.res).each(function(index) {
                var id = this.id;
                var title = this.title;
                var status = this.status;
                var statusText = (status) ? 'В работе' : 'Открыта'
                var description = this.description;
                var newTask = $('<div class="task">\
                <div class="t-id">' + id + '</div>\
                <div class="t-title">' + title + '</div>\
                <div class="t-status" value="'+status+'">'+statusText+'</div>\
                <div class="t-description">' + description + '</div>\
                </div>')
                taskList.prepend(newTask);
                reloadEvent();
            })
        }
    });

    reloadEvent();
});